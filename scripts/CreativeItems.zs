print("loading creative_items.zs");

# --------Magic Changes--------


# The Everlasting Guilty Pool
 
#currently all commented out until we decide on creative items

# recipes.addShaped(<botania:pool:1>,
# [[<botania:manaringgreater>.withTag({mana:2000000}), <aetherworks:block_aether>, <botania:manaringgreater>.withTag({mana:2000000})],
# [<thermalfoundation:storage:8>, <astralsorcery:blockaltar:3>, <thermalfoundation:storage:8>],
# [<botania:manaringgreater>.withTag({mana:2000000}), <aetherworks:block_aether>, <botania:manaringgreater>.withTag({mana:2000000})]]);

# Creative Ember Source 

#currently all commented out until we decide on creative items

# recipes.addShaped(<embers:creative_ember_source>,
# [[<botania:manaresource:5>, <bloodarsenal:slate:4>, <botania:manaresource:5>],
# [<botania:manaresource:5>, <embers:flame_barrier>, <botania:manaresource:5>],
# [<botania:manaresource:5>, <bloodarsenal:slate:4>, <botania:manaresource:5>]]);

# Flux Sponge

recipes.addShaped(<thaumcraft:creative_flux_sponge>, [[<minecraft:sponge>,   <botania:rune:13>,       <minecraft:sponge>],
                                                     [<soot:metallurgic_dust>, <extrautils2:opinium:8>, <soot:metallurgic_dust>],
                                                     [<minecraft:sponge>,    <botania:rune:15>,       <minecraft:sponge>]]);