# Authors: JahYi, Moji

import crafttweaker.item.IItemStack;

#---------------------------------------------------------------------------------------------

# Laser drill takes iridium ingots

var machineFrames = [<teslacorelib:machine_case>, <enderio:item_material>, <thermalexpansion:frame>, <mekanism:basicblock:8>] as IItemStack[];
<ore:laserLens>.add(<industrialforegoing:laser_lens:*>);
<ore:laserLens>.add(<industrialforegoing:laser_lens_inverted:*>);

recipes.remove(<industrialforegoing:laser_drill>);
recipes.remove(<industrialforegoing:laser_base>);

for frame in machineFrames {
	recipes.addShaped(<industrialforegoing:laser_drill>,
		[[<ore:itemRubber>,   <ore:laserLens>, <ore:itemRubber>],
	     [<ore:gearDiamond>,  <ore:glowstone>, <ore:gearDiamond>],
	     [<ore:ingotIridium>, frame,      	   <ore:ingotIridium>]]);
		 
	recipes.addShaped(<industrialforegoing:laser_base>,
		[[<ore:itemRubber>,   <ore:glowstone>, <ore:itemRubber>],
		 [<ore:gearGold>, 	  <ore:glowstone>, <ore:gearGold>],
		 [<ore:ingotIridium>, frame, 		   <ore:ingotIridium>]]);
}