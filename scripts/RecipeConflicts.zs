print("loading recipe_conflicts.zs");

# ------Removing Conflicting Metal Tools------

# Silver
mods.jei.JEI.removeAndHide(<embers:pickaxe_silver>);
mods.jei.JEI.removeAndHide(<embers:axe_silver>);
mods.jei.JEI.removeAndHide(<embers:shovel_silver>);
mods.jei.JEI.removeAndHide(<embers:hoe_silver>);
mods.jei.JEI.removeAndHide(<embers:sword_silver>);
mods.jei.JEI.removeAndHide(<iceandfire:silver_pickaxe>);
mods.jei.JEI.removeAndHide(<iceandfire:silver_axe>);
mods.jei.JEI.removeAndHide(<iceandfire:silver_shovel>);
mods.jei.JEI.removeAndHide(<iceandfire:silver_hoe>);
mods.jei.JEI.removeAndHide(<iceandfire:silver_sword>);
mods.jei.JEI.removeAndHide(<iceandfire:armor_silver_metal_helmet>);
mods.jei.JEI.removeAndHide(<iceandfire:armor_silver_metal_chestplate>);
mods.jei.JEI.removeAndHide(<iceandfire:armor_silver_metal_leggings>);
mods.jei.JEI.removeAndHide(<iceandfire:armor_silver_metal_boots>);
# Tin
mods.jei.JEI.removeAndHide(<embers:pickaxe_tin>);
mods.jei.JEI.removeAndHide(<embers:axe_tin>);
mods.jei.JEI.removeAndHide(<embers:shovel_tin>);
mods.jei.JEI.removeAndHide(<embers:hoe_tin>);
mods.jei.JEI.removeAndHide(<embers:sword_tin>);
# Steel
mods.jei.JEI.removeAndHide(<mekanismtools:steelpickaxe>);
mods.jei.JEI.removeAndHide(<mekanismtools:steelaxe>);
mods.jei.JEI.removeAndHide(<mekanismtools:steelshovel>);
mods.jei.JEI.removeAndHide(<mekanismtools:steelhoe>);
mods.jei.JEI.removeAndHide(<mekanismtools:steelsword>);
mods.jei.JEI.removeAndHide(<mekanismtools:steelhelmet>);
mods.jei.JEI.removeAndHide(<mekanismtools:steelchestplate>);
mods.jei.JEI.removeAndHide(<mekanismtools:steelleggings>);
mods.jei.JEI.removeAndHide(<mekanismtools:steelboots>);
mods.jei.JEI.removeAndHide(<immersiveengineering:pickaxe_steel>);
mods.jei.JEI.removeAndHide(<immersiveengineering:axe_steel>);
mods.jei.JEI.removeAndHide(<immersiveengineering:shovel_steel>);
mods.jei.JEI.removeAndHide(<immersiveengineering:sword_steel>);
# Bronze
mods.jei.JEI.removeAndHide(<embers:pickaxe_bronze>);
mods.jei.JEI.removeAndHide(<embers:axe_bronze>);
mods.jei.JEI.removeAndHide(<embers:shovel_bronze>);
mods.jei.JEI.removeAndHide(<embers:hoe_bronze>);
mods.jei.JEI.removeAndHide(<embers:sword_bronze>);
mods.jei.JEI.removeAndHide(<mekanismtools:bronzepickaxe>);
mods.jei.JEI.removeAndHide(<mekanismtools:bronzeaxe>);
mods.jei.JEI.removeAndHide(<mekanismtools:bronzeshovel>);
mods.jei.JEI.removeAndHide(<mekanismtools:bronzehoe>);
mods.jei.JEI.removeAndHide(<mekanismtools:bronzesword>);
mods.jei.JEI.removeAndHide(<mekanismtools:bronzehelmet>);
mods.jei.JEI.removeAndHide(<mekanismtools:bronzechestplate>);
mods.jei.JEI.removeAndHide(<mekanismtools:bronzeleggings>);
mods.jei.JEI.removeAndHide(<mekanismtools:bronzeboots>);
mods.jei.JEI.removeAndHide(<techreborn:bronzepickaxe>);
mods.jei.JEI.removeAndHide(<techreborn:bronzeaxe>);
mods.jei.JEI.removeAndHide(<techreborn:bronzespade>);
mods.jei.JEI.removeAndHide(<techreborn:bronzehoe>);
mods.jei.JEI.removeAndHide(<techreborn:bronzesword>);
mods.jei.JEI.removeAndHide(<techreborn:bronzehelmet>);
mods.jei.JEI.removeAndHide(<techreborn:bronzechestplate>);
mods.jei.JEI.removeAndHide(<techreborn:bronzeleggings>);
mods.jei.JEI.removeAndHide(<techreborn:bronzeboots>);
# Lead
mods.jei.JEI.removeAndHide(<embers:pickaxe_lead>);
mods.jei.JEI.removeAndHide(<embers:axe_lead>);
mods.jei.JEI.removeAndHide(<embers:shovel_lead>);
mods.jei.JEI.removeAndHide(<embers:hoe_lead>);
mods.jei.JEI.removeAndHide(<embers:sword_lead>);
# Electrum
mods.jei.JEI.removeAndHide(<embers:pickaxe_electrum>);
mods.jei.JEI.removeAndHide(<embers:axe_electrum>);
mods.jei.JEI.removeAndHide(<embers:shovel_electrum>);
mods.jei.JEI.removeAndHide(<embers:hoe_electrum>);
mods.jei.JEI.removeAndHide(<embers:sword_electrum>);
# Aluminum
mods.jei.JEI.removeAndHide(<embers:pickaxe_aluminum>);
mods.jei.JEI.removeAndHide(<embers:axe_aluminum>);
mods.jei.JEI.removeAndHide(<embers:shovel_aluminum>);
mods.jei.JEI.removeAndHide(<embers:hoe_aluminum>);
mods.jei.JEI.removeAndHide(<embers:sword_aluminum>);
# Copper
mods.jei.JEI.removeAndHide(<embers:pickaxe_copper>);
mods.jei.JEI.removeAndHide(<embers:axe_copper>);
mods.jei.JEI.removeAndHide(<embers:shovel_copper>);
mods.jei.JEI.removeAndHide(<embers:hoe_copper>);
mods.jei.JEI.removeAndHide(<embers:sword_copper>);
# Nickel
mods.jei.JEI.removeAndHide(<embers:pickaxe_nickel>);
mods.jei.JEI.removeAndHide(<embers:axe_nickel>);
mods.jei.JEI.removeAndHide(<embers:shovel_nickel>);
mods.jei.JEI.removeAndHide(<embers:hoe_nickel>);
mods.jei.JEI.removeAndHide(<embers:sword_nickel>);
# Invar
mods.jei.JEI.removeAndHide(<embers:pickaxe_nickel>);
mods.jei.JEI.removeAndHide(<embers:axe_nickel>);
mods.jei.JEI.removeAndHide(<embers:shovel_nickel>);
mods.jei.JEI.removeAndHide(<embers:hoe_nickel>);
mods.jei.JEI.removeAndHide(<embers:sword_nickel>);

# Paxel fix because they don't use mekanism tools now
recipes.remove(<mekanismtools:steelpaxel>);
recipes.addShaped(<mekanismtools:steelpaxel>, [[<thermalfoundation:tool.axe_steel>, <thermalfoundation:tool.pickaxe_steel>, <thermalfoundation:tool.shovel_steel>],
                                              [null,                                <minecraft:iron_ingot>],
                                              [null,                                <minecraft:iron_ingot>]]);

recipes.remove(<mekanismtools:bronzepaxel>);
recipes.addShaped(<mekanismtools:bronzepaxel>, [[<thermalfoundation:tool.axe_bronze>, <thermalfoundation:tool.pickaxe_bronze>, <thermalfoundation:tool.shovel_bronze>],
                                               [null,                                 <minecraft:iron_ingot>],
                                               [null,                                 <minecraft:iron_ingot>]]);

 # ------Gears------

 # Wood Gear
mods.jei.JEI.removeAndHide(<enderio:item_material:9>);
recipes.remove(<teslacorelib:gear_wood>);
recipes.addShaped(<teslacorelib:gear_wood>, [[null,         <ore:plankWood>],
                                            [<ore:plankWood>, <ore:stickWood>, <ore:plankWood>],
                                            [null,          <ore:plankWood>]]);
 # Stone Gear
mods.jei.JEI.removeAndHide(<enderio:item_material:10>);
recipes.remove(<teslacorelib:gear_stone>);
recipes.addShaped(<teslacorelib:gear_stone>, [[null,           <ore:cobblestone>],
                                             [<ore:cobblestone>, <teslacorelib:gear_wood>, <ore:cobblestone>],
                                             [null,            <ore:cobblestone>]]);