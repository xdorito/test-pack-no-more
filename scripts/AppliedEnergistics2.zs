# Author: Moji

import mods.appliedenergistics2.Inscriber;

#--------------------------------------------------------------------------------------------

# Recipes for inscriber presses

var InscriberPressCalculation = <appliedenergistics2:material:13>;
var InscriberPressEngineering = <appliedenergistics2:material:14>;
var InscriberPressLogic = <appliedenergistics2:material:15>;
var InscriberPressSilicon = <appliedenergistics2:material:19>;

Inscriber.addRecipe(InscriberPressCalculation, <minecraft:iron_block>, false, <appliedenergistics2:material:10>);
Inscriber.addRecipe(InscriberPressEngineering, <minecraft:iron_block>, false, <minecraft:diamond>);
Inscriber.addRecipe(InscriberPressLogic, <minecraft:iron_block>, false, <minecraft:gold_ingot>);
Inscriber.addRecipe(InscriberPressSilicon, <minecraft:iron_block>, false, <enderio:item_material:5>);
Inscriber.addRecipe(InscriberPressSilicon, <minecraft:iron_block>, false, <refinedstorage:silicon>);

# Printed silicon from Refined Storage silicon

Inscriber.addRecipe(<appliedenergistics2:material:20>, <refinedstorage:silicon>, true, InscriberPressSilicon);