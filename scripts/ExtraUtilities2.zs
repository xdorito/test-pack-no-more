# Authors: JahYi, Moji

# Quantum quarry takes iridium alloy plates and unstable ingots

recipes.remove(<extrautils2:quarry>);
recipes.addShaped(<extrautils2:quarry>,
	[[<techreborn:plates:37>,       <extrautils2:unstableingots>, <techreborn:plates:37>],
	 [<extrautils2:unstableingots>, <minecraft:nether_star>, 	  <extrautils2:unstableingots>],
	 [<techreborn:plates:37>,       <extrautils2:unstableingots>, <techreborn:plates:37>]]);
	 
recipes.remove(<extrautils2:quarryproxy>);
recipes.addShaped(<extrautils2:quarryproxy>, 
	[[<ore:endstone>, 				   <minecraft:end_rod>, 			<ore:endstone>],
	 [<techreborn:plates:37>, 		   <minecraft:diamond_pickaxe>, 	<techreborn:plates:37>],
	 [<extrautils2:decorativesolid:3>, <extrautils2:decorativesolid:3>, <extrautils2:decorativesolid:3>]]);
	 
# Deep dark portal takes iridium alloy plates, 8x compressed cobble, and evil infused iron

recipes.remove(<extrautils2:teleporter:1>);
recipes.addShaped(<extrautils2:teleporter:1>,
	[[<techreborn:plates:37>, 			<extrautils2:simpledecorative:2>, 	   <techreborn:plates:37>],
	 [<extrautils2:simpledecorative:2>, <extrautils2:compressedcobblestone:7>, <extrautils2:simpledecorative:2>],
	 [<techreborn:plates:37>,			<extrautils2:simpledecorative:2>, 	   <techreborn:plates:37>]]);