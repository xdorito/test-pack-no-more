# Authors: Moji, JahYi

import crafttweaker.item.IItemStack;

#---------------------------------------------------------------------------------------------

# Fusion reactor components take iridium alloy plates

recipes.remove(<mekanismgenerators:reactor:1>);
recipes.addShaped(<mekanismgenerators:reactor:1> * 4,
	[[null, 				   <mekanism:basicblock:8>],
     [<mekanism:basicblock:8>, <techreborn:plates:37>,  <mekanism:basicblock:8>],
     [null, 				   <mekanism:basicblock:8>]]);
	 
recipes.remove(<mekanismgenerators:reactor>);
recipes.addShaped(<mekanismgenerators:reactor>,
	[[<mekanismgenerators:reactor:1>, <techreborn:plates:37>, <mekanismgenerators:reactor:1>],
	 [<ore:circuitUltimate>, 		  <mekanism:gastank>, 	  <ore:circuitUltimate>],
	 [<mekanismgenerators:reactor:1>, <techreborn:plates:37>, <mekanismgenerators:reactor:1>]]);

# Saplings, ore:crop* (except berries), and eggs produce 1 biofuel

mods.mekanism.crusher.removeRecipe(<mekanism:biofuel>);

val biofuelItems = [<minecraft:pumpkin>, <minecraft:melon_block>, <minecraft:wheat>, <minecraft:nether_wart>, <minecraft:carrot>, <minecraft:potato>, <actuallyadditions:item_misc:13>, <actuallyadditions:item_food:16>,
					<actuallyadditions:item_coffee_beans>, <natura:materials>, <natura:materials:3>, <minecraft:egg>, <minecraft:sapling>, <minecraft:sapling:1>, <minecraft:sapling:2>, <minecraft:sapling:3>,
					<minecraft:sapling:4>, <minecraft:sapling:5>, <animus:blockbloodsapling>, <integrateddynamics:menril_sapling>, <natura:overworld_sapling>, <natura:overworld_sapling:1>, <natura:overworld_sapling:2>,
					<natura:overworld_sapling:3>, <natura:overworld_sapling2>, <natura:overworld_sapling2:1>, <natura:overworld_sapling2:2>, <natura:overworld_sapling2:3>, <natura:redwood_sapling>, <natura:nether_sapling>,
					<natura:nether_sapling:1>, <natura:nether_sapling:2>, <natura:nether_sapling2>, <techreborn:rubber_sapling>, <thaumcraft:sapling_greatwood>, <thaumcraft:sapling_silverwood>, <traverse:red_autumnal_sapling>,
					<traverse:brown_autumnal_sapling>, <traverse:orange_autumnal_sapling>, <traverse:yellow_autumnal_sapling>, <traverse:fir_sapling>] as IItemStack[];

for bioItem in biofuelItems {
	mods.mekanism.crusher.addRecipe(bioItem, <mekanism:biofuel>);
}