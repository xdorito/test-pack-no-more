# Author: Thanatos

# Sigil of divinity is much more expensive

recipes.remove(<bloodarsenal:base_item:9>);
recipes.removeShaped(<bloodarsenal:base_item:9>);
recipes.removeShapeless(<bloodarsenal:base_item:9>);
recipes.addShaped(<bloodarsenal:sigil_divinity>,
	[[<bloodarsenal:blood_diamond:2>, <botania:manaresource:14>,        <bloodarsenal:blood_diamond:2>],
	 [<bloodarsenal:base_item:8>,     <thaumcraft:causality_collapser>, <bloodarsenal:base_item:8>],
	 [<bloodarsenal:blood_diamond:2>, <astralsorcery:itemshiftingstar>, <bloodarsenal:blood_diamond:2>]]);