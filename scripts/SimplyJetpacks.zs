# Author: Moji

import crafttweaker.item.IItemStack;

#----------------------------------------------------------------------------

# Removing jetplates

var jetplateItems = [<simplyjetpacks:itemjetpack:9>, <simplyjetpacks:itemjetpack:18>, <simplyjetpacks:metaitemmods:1>, <simplyjetpacks:metaitemmods:2>, <simplyjetpacks:metaitemmods:8>, <simplyjetpacks:metaitemmods:15>,
					 <simplyjetpacks:metaitemmods:17>, <simplyjetpacks:metaitemmods:27>] as IItemStack[];

for item in jetplateItems {
	recipes.remove(item);
}

mods.thermalexpansion.Transposer.removeFillRecipe(<simplyjetpacks:metaitemmods:15>, <liquid:glowstone>);
mods.thermalexpansion.Transposer.removeFillRecipe(<simplyjetpacks:metaitemmods:17>, <liquid:cryotheum>);

# Simplifying thruster recipes

var thrusters = [<simplyjetpacks:metaitemmods:4>, <simplyjetpacks:metaitemmods:5>, <simplyjetpacks:metaitemmods:6>, <simplyjetpacks:metaitemmods:7>, <simplyjetpacks:metaitemmods:23>, <simplyjetpacks:metaitemmods:24>,
				 <simplyjetpacks:metaitemmods:25>, <simplyjetpacks:metaitemmods:26>] as IItemStack[];

for item in thrusters {
	recipes.remove(item);
}

## Ender IO

recipes.addShaped(<simplyjetpacks:metaitemmods:4>,
	[[null, 					 <ore:ingotConductiveIron>],
	 [<ore:gearStone>, 			 <enderio:item_basic_capacitor>, <ore:gearStone>],
	 [<ore:ingotConductiveIron>, <ore:dustBedrock>, 			 <ore:ingotConductiveIron>]]);
													
recipes.addShaped(<simplyjetpacks:metaitemmods:5>,
	[[null, 				 	  <ore:ingotElectricalSteel>],
	 [<ore:gearIronInfinity>, 	  <enderio:item_basic_capacitor>, <ore:gearIronInfinity>],
	 [<ore:ingotElectricalSteel>, <minecraft:blaze_powder>,  	  <ore:ingotElectricalSteel>]]);
													
recipes.addShaped(<simplyjetpacks:metaitemmods:6>,
	[[null, 					 <ore:ingotEnergeticAlloy>],
	 [<ore:gearEnergized>, 		 <enderio:item_basic_capacitor:1>, <ore:gearEnergized>],
	 [<ore:ingotEnergeticAlloy>, <ore:itemPulsatingCrystal>, 	   <ore:ingotEnergeticAlloy>]]);
													
recipes.addShaped(<simplyjetpacks:metaitemmods:7>,
	[[null, 				   <ore:ingotVibrantAlloy>],
	 [<ore:gearVibrant>, 	   <enderio:item_basic_capacitor:2>, <ore:gearVibrant>],
	 [<ore:ingotVibrantAlloy>, <ore:itemVibrantCrystal>, 		 <ore:ingotVibrantAlloy>]]);
													
## Thermal

recipes.addShaped(<simplyjetpacks:metaitemmods:23>,
	[[null, 			<ore:ingotLead>],
	 [<ore:gearCopper>, <thermalfoundation:material:513>, <ore:gearCopper>],
	 [<ore:ingotLead>, 	<ore:dustRedstone>, 			  <ore:ingotLead>]]);
													 
recipes.addShaped(<simplyjetpacks:metaitemmods:24>,
	[[null, 			<ore:ingotInvar>],
	 [<ore:gearBronze>, <thermalfoundation:material:513>, <ore:gearBronze>],
	 [<ore:ingotInvar>, <ore:dustBlaze>, 				  <ore:ingotInvar>]]);
													 
recipes.addShaped(<simplyjetpacks:metaitemmods:25>,
	[[null, 			   <ore:ingotElectrum>],
	 [<ore:gearSilver>,    <thermalfoundation:material:513>, <ore:gearSilver>],
	 [<ore:ingotElectrum>, <ore:dustCryotheum>, 			 <ore:ingotElectrum>]]);
													 
recipes.addShaped(<simplyjetpacks:metaitemmods:26>,
	[[null, 			   <ore:ingotEnderium>],
	 [<ore:gearLumium>,    <thermalfoundation:material:513>, <ore:gearLumium>],
	 [<ore:ingotEnderium>, <ore:dustPyrotheum>, 			 <ore:ingotEnderium>]]);
													 
# Simplifying jetpack recipes

var jetpacks = [<simplyjetpacks:itemjetpack:2>, <simplyjetpacks:itemjetpack:3>, <simplyjetpacks:itemjetpack:4>, <simplyjetpacks:itemjetpack:11>, <simplyjetpacks:itemjetpack:12>, <simplyjetpacks:itemjetpack:13>] as IItemStack[];		
				
for item in jetpacks {
	recipes.remove(item);
}

## Ender IO
### From scratch

recipes.addShaped(<simplyjetpacks:itemjetpack:2>,
	[[<ore:ingotElectricalSteel>, 	   <enderio:item_basic_capacitor>, <ore:ingotElectricalSteel>],
     [<ore:ingotElectricalSteel>, 	   <simplyjetpacks:metaitem:4>,    <ore:ingotElectricalSteel>],
     [<simplyjetpacks:metaitemmods:5>, null, 						   <simplyjetpacks:metaitemmods:5>]]);
												   
recipes.addShaped(<simplyjetpacks:itemjetpack:3>,
	[[<ore:ingotEnergeticAlloy>, 	   <enderio:item_basic_capacitor:1>, <ore:ingotEnergeticAlloy>],
	 [<ore:ingotEnergeticAlloy>, 	   <simplyjetpacks:metaitem:4>, 	 <ore:ingotEnergeticAlloy>],
	 [<simplyjetpacks:metaitemmods:6>, null, 							 <simplyjetpacks:metaitemmods:6>]]);
												   
recipes.addShaped(<simplyjetpacks:itemjetpack:4>,
	[[<ore:ingotVibrantAlloy>, 	 	   <enderio:item_basic_capacitor:2>, <ore:ingotVibrantAlloy>],
     [<ore:ingotVibrantAlloy>, 	 	   <simplyjetpacks:metaitem:4>, 	 <ore:ingotVibrantAlloy>],
     [<simplyjetpacks:metaitemmods:7>, null, 							 <simplyjetpacks:metaitemmods:7>]]);
												   
### Upgrading
	
recipes.addShaped("jetpack_upgrade_eio_t2", <simplyjetpacks:itemjetpack:2>,
	[[null, 	 					   <ore:dustBedrock>],
     [<ore:ingotElectricalSteel>, 	   <simplyjetpacks:itemjetpack:1>.marked("jetpack"), <ore:ingotElectricalSteel>],
     [<simplyjetpacks:metaitemmods:5>, null, 						 	 				 <simplyjetpacks:metaitemmods:5>]],
    function(out, ins, cInfo) {
	    return out.withTag(ins.jetpack.tag);
    }, null);
	
recipes.addShaped("jetpack_upgrade_eio_t3", <simplyjetpacks:itemjetpack:3>,
	[[null, 	 					   <enderio:item_basic_capacitor>],
     [<ore:ingotEnergeticAlloy>, 	   <simplyjetpacks:itemjetpack:2>.marked("jetpack"), <ore:ingotEnergeticAlloy>],
     [<simplyjetpacks:metaitemmods:6>, null, 						 	 				 <simplyjetpacks:metaitemmods:6>]],
    function(out, ins, cInfo) {
	    return out.withTag(ins.jetpack.tag);
    }, null);
	
recipes.addShaped("jetpack_upgrade_eio_t4", <simplyjetpacks:itemjetpack:4>,
	[[null, 	 					   <enderio:item_basic_capacitor>],
     [<ore:ingotVibrantAlloy>, 	   	   <simplyjetpacks:itemjetpack:3>.marked("jetpack"), <ore:ingotVibrantAlloy>],
     [<simplyjetpacks:metaitemmods:7>, null, 						 	 				 <simplyjetpacks:metaitemmods:7>]],
    function(out, ins, cInfo) {
	    return out.withTag(ins.jetpack.tag);
    }, null);

## Thermal
### From scratch

recipes.addShaped(<simplyjetpacks:itemjetpack:11>,
	[[<ore:ingotInvar>, 				<thermalexpansion:capacitor:1>, <ore:ingotInvar>],
	 [<ore:ingotInvar>, 				<simplyjetpacks:metaitem:4>,    <ore:ingotInvar>],
	 [<simplyjetpacks:metaitemmods:24>, null, 						    <simplyjetpacks:metaitemmods:24>]]);
												
recipes.addShaped(<simplyjetpacks:itemjetpack:12>,
	[[<ore:ingotElectrum>, 			    <thermalexpansion:capacitor:2>, <ore:ingotElectrum>],
	 [<ore:ingotElectrum>, 			    <simplyjetpacks:metaitem:4>,	<ore:ingotElectrum>],
	 [<simplyjetpacks:metaitemmods:25>, null, 						    <simplyjetpacks:metaitemmods:25>]]);
													
recipes.addShaped(<simplyjetpacks:itemjetpack:13>,
	[[<ore:ingotEnderium>, 			    <thermalexpansion:capacitor:4>, <ore:ingotEnderium>],
	 [<ore:ingotEnderium>, 			    <simplyjetpacks:metaitem:4>,    <ore:ingotEnderium>],
	 [<simplyjetpacks:metaitemmods:26>, null, 						    <simplyjetpacks:metaitemmods:26>]]);

### Upgrading

recipes.addShaped("jetpack_upgrade_thermal_t2", <simplyjetpacks:itemjetpack:11>,
	[[null, 	 					   	<thermalfoundation:material:513>],
     [<ore:ingotInvar>, 	   			<simplyjetpacks:itemjetpack:10>.marked("jetpack"), <ore:ingotInvar>],
     [<simplyjetpacks:metaitemmods:24>, null, 						 	 				   <simplyjetpacks:metaitemmods:24>]],
    function(out, ins, cInfo) {
	    return out.withTag(ins.jetpack.tag);
    }, null);
	
recipes.addShaped("jetpack_upgrade_thermal_t3", <simplyjetpacks:itemjetpack:12>,
	[[null, 	 					   	<thermalfoundation:material:513>],
     [<ore:ingotElectrum>, 	   			<simplyjetpacks:itemjetpack:11>.marked("jetpack"), <ore:ingotElectrum>],
     [<simplyjetpacks:metaitemmods:25>, null, 						 	 				   <simplyjetpacks:metaitemmods:25>]],
    function(out, ins, cInfo) {
	    return out.withTag(ins.jetpack.tag);
    }, null);
	
recipes.addShaped("jetpack_upgrade_thermal_t4", <simplyjetpacks:itemjetpack:13>,
	[[null, 	 					   	<thermalfoundation:material:513>],
     [<ore:ingotEnderium>, 	   			<simplyjetpacks:itemjetpack:12>.marked("jetpack"), <ore:ingotEnderium>],
     [<simplyjetpacks:metaitemmods:26>, null, 						 	 				   <simplyjetpacks:metaitemmods:26>]],
    function(out, ins, cInfo) {
	    return out.withTag(ins.jetpack.tag);
    }, null);