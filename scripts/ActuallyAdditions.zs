# Authors: Moji, JahYi

print("loading actually_additions.zs");

# Lens of the Miner takes iridium ingots

recipes.removeShaped(<actuallyadditions:item_mining_lens>);
recipes.addShaped(<actuallyadditions:item_mining_lens>,
	[[<ore:ingotIridium>, <ore:ingotGold>,                  <ore:ingotIridium>],
	 [<ore:gemDiamond>,   <actuallyadditions:item_misc:18>, <ore:gemEmerald>],
	 [<ore:ingotIridium>, <ore:ingotPlatinum>,              <ore:ingotIridium>]]);