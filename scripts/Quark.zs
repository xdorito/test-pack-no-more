#Allowing you to convert Quark chests to normal ones

#Author: JahYi

var quarkChests = [<quark:custom_chest>, <quark:custom_chest:1>, <quark:custom_chest:2>, <quark:custom_chest:3>, <quark:custom_chest:4>]

for chestBoi in quarkChests {
    recipes.addshapeless(<minecraft:chest>, chestBoi);
}
