# Author: Moji

import crafttweaker.item.IItemStack;

#-----------------------------------------------------------------------------------

# Fusion reactor components take iridium alloy plates

recipes.remove(<nuclearcraft:fusion_core>);
recipes.addShaped(<nuclearcraft:fusion_core>, [[<techreborn:plates:37>, 			  <nuclearcraft:part:5>,  <techreborn:plates:37>],
											   [<nuclearcraft:chemical_reactor_idle>, <nuclearcraft:part:10>, <nuclearcraft:chemical_reactor_idle>],
											   [<techreborn:plates:37>, 			  <nuclearcraft:part:5>,  <techreborn:plates:37>]]);
											   
recipes.remove(<nuclearcraft:fusion_connector>);
recipes.addShaped(<nuclearcraft:fusion_connector>, [[<ore:ingotTough>, <ore:plateBasic>, 	   <ore:ingotTough>],
													[<ore:plateBasic>, <techreborn:plates:37>, <ore:plateBasic>],
													[<ore:ingotTough>, <ore:plateBasic>, 	   <ore:ingotTough>]]);

# Recipes for graphite dust from pulverized coal in various crushers

mods.thermalexpansion.Pulverizer.addRecipe(<nuclearcraft:dust:8>, <thermalfoundation:material:768>, 2000);
mods.actuallyadditions.Crusher.addRecipe(<nuclearcraft:dust:8>, <thermalfoundation:material:768>);
mods.immersiveengineering.Crusher.addRecipe(<nuclearcraft:dust:8>, <thermalfoundation:material:768>, 2000);
mods.mekanism.enrichment.addRecipe(<nuclearcraft:dust:8>, <thermalfoundation:material:768>);
mods.extrautils2.Crusher.add(<nuclearcraft:dust:8>, <thermalfoundation:material:768>);
mods.techreborn.grinder.addRecipe(<nuclearcraft:dust:8>, <thermalfoundation:material:768>, 240, 8);

# Pressurizer recipes in TE compactor and TR compressor

mods.thermalexpansion.Compactor.addPressRecipe(<nuclearcraft:gem:1>, <nuclearcraft:gem_dust:4>, 4000);
mods.thermalexpansion.Compactor.addPressRecipe(<minecraft:diamond>, <nuclearcraft:ingot:8> * 64, 40000);
mods.thermalexpansion.Compactor.addPressRecipe(<techreborn:plates:2>, <techreborn:part:34>, 4000);
mods.techreborn.compressor.addRecipe(<nuclearcraft:gem:1>, <nuclearcraft:gem_dust:4>, 400, 80);
mods.techreborn.compressor.addRecipe(<minecraft:diamond>, <nuclearcraft:ingot:8> * 64, 400, 80);

# Recipes for tough alloy in various alloy furnaces

var materialLithium = [<mekanism:otherdust:4>, <nuclearcraft:dust:6>, <nuclearcraft:ingot:6>] as IItemStack[];

for lithium in materialLithium {
	mods.thermalexpansion.InductionSmelter.addRecipe(<nuclearcraft:alloy:1> * 2, <nuclearcraft:alloy:6>, lithium, 2400);
}

for lithium in materialLithium {
	mods.immersiveengineering.ArcFurnace.addRecipe(<nuclearcraft:alloy:1> * 2, <nuclearcraft:alloy:6>, null, 200, 512, [lithium]);
}

for lithium in materialLithium {
	mods.immersiveengineering.AlloySmelter.addRecipe(<nuclearcraft:alloy:1> * 2, <nuclearcraft:alloy:6>, lithium, 200);
}

for lithium in materialLithium {
	mods.techreborn.alloySmelter.addRecipe(<nuclearcraft:alloy:1> * 2, <nuclearcraft:alloy:6>, lithium, 200, 60);
}

# Recipes for hard carbon alloy in various alloy furnaces

var materialGraphite = [<nuclearcraft:dust:8>, <nuclearcraft:ingot:8>] as IItemStack[];
var materialDiamond = [<minecraft:diamond>, <mekanism:otherdust>, <nuclearcraft:gem_dust>, <techreborn:dust:16>, <actuallyadditions:item_dust:2>] as IItemStack[];

for graphite in materialGraphite {
	for diamond in materialDiamond {
		mods.thermalexpansion.InductionSmelter.addRecipe(<nuclearcraft:alloy:2> * 2, graphite * 2, diamond, 2400);
	}
}

for graphite in materialGraphite {
	for diamond in materialDiamond {
		mods.immersiveengineering.ArcFurnace.addRecipe(<nuclearcraft:alloy:2> * 2, graphite * 2, null, 200, 512, [diamond]);
	}
}

for graphite in materialGraphite {
	for diamond in materialDiamond {
		mods.immersiveengineering.AlloySmelter.addRecipe(<nuclearcraft:alloy:2> * 2, graphite * 2, diamond, 200);
	}
}

for graphite in materialGraphite {
	for diamond in materialDiamond {
		mods.techreborn.alloySmelter.addRecipe(<nuclearcraft:alloy:2> * 2, graphite * 2, diamond, 200, 60);
	}
}

# Recipes for magnesium diboride alloy in various alloy furnaces

var materialMagnesium = [<nuclearcraft:dust:7>, <nuclearcraft:ingot:7>, <techreborn:dust:30>] as IItemStack[];
var materialBoron = [<nuclearcraft:dust:5>, <nuclearcraft:ingot:5>] as IItemStack[];

for magnesium in materialMagnesium {
	for boron in materialBoron {
		mods.thermalexpansion.InductionSmelter.addRecipe(<nuclearcraft:alloy:3> * 3, magnesium, boron * 2, 2400);
	}
}

for magnesium in materialMagnesium {
	for boron in materialBoron {
		mods.immersiveengineering.ArcFurnace.addRecipe(<nuclearcraft:alloy:3> * 3, magnesium, null, 200, 512, [boron * 2]);
	}
}

for magnesium in materialMagnesium {
	for boron in materialBoron {
		mods.immersiveengineering.AlloySmelter.addRecipe(<nuclearcraft:alloy:3> * 3, magnesium, boron * 2, 200);
	}
}

for magnesium in materialMagnesium {
	for boron in materialBoron {
		mods.techreborn.alloySmelter.addRecipe(<nuclearcraft:alloy:3> * 3, magnesium, boron * 2, 200, 60);
	}
}

# Recipes for lithium manganese dioxide alloy in various alloy furnaces

var materialManganeseDioxide = [<nuclearcraft:dust_oxide:3>, <nuclearcraft:ingot_oxide:3>] as IItemStack[];

for lithium in materialLithium {
	for manganeseDioxide in materialManganeseDioxide {
		mods.thermalexpansion.InductionSmelter.addRecipe(<nuclearcraft:alloy:4> * 2, lithium, manganeseDioxide, 2400);
	}
}

for lithium in materialLithium {
	for manganeseDioxide in materialManganeseDioxide {
		mods.immersiveengineering.ArcFurnace.addRecipe(<nuclearcraft:alloy:4> * 2, lithium, null, 200, 512, [manganeseDioxide]);
	}
}

for lithium in materialLithium {
	for manganeseDioxide in materialManganeseDioxide {
		mods.immersiveengineering.AlloySmelter.addRecipe(<nuclearcraft:alloy:4> * 2, lithium, manganeseDioxide, 200);
	}
}

for lithium in materialLithium {
	for manganeseDioxide in materialManganeseDioxide {
		mods.techreborn.alloySmelter.addRecipe(<nuclearcraft:alloy:4> * 2, lithium, manganeseDioxide, 200, 60);
	}
}

# Recipes for ferroboron alloy in various alloy furnaces

var materialSteel = [<thermalfoundation:material:96>, <thermalfoundation:material:160>] as IItemStack[];

for steel in materialSteel {
	for boron in materialBoron {
		mods.thermalexpansion.InductionSmelter.addRecipe(<nuclearcraft:alloy:6> * 2, steel, boron, 2400);
	}
}

for steel in materialSteel {
	for boron in materialBoron {
		mods.immersiveengineering.ArcFurnace.addRecipe(<nuclearcraft:alloy:6> * 2, steel, null, 200, 512, [boron]);
	}
}

for steel in materialSteel {
	for boron in materialBoron {
		mods.immersiveengineering.AlloySmelter.addRecipe(<nuclearcraft:alloy:6> * 2, steel, boron, 200);
	}
}

for steel in materialSteel {
	for boron in materialBoron {
		mods.techreborn.alloySmelter.addRecipe(<nuclearcraft:alloy:6> * 2, steel, boron, 200, 60);
	}
}

# Fluid infuser recipes in TE fluid transposer

mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:ingot_oxide>, <nuclearcraft:ingot:3>, <liquid:oxygen> * 1000, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:ingot_oxide:1>, <immersiveengineering:metal:5>, <liquid:oxygen> * 1000, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:ingot_oxide:3>, <nuclearcraft:ingot_oxide:2>, <liquid:oxygen> * 1000, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:dust_oxide>, <nuclearcraft:dust:3>, <liquid:oxygen> * 1000, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:dust_oxide:1>, <immersiveengineering:metal:14>, <liquid:oxygen> * 1000, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:dust_oxide:2>, <techreborn:dust:31>, <liquid:oxygen> * 1000, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:dust_oxide:3>, <nuclearcraft:dust_oxide:2>, <liquid:oxygen> * 1000, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:block_ice>, <minecraft:ice>, <liquid:helium> * 50, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:block_ice>, <minecraft:packed_ice>, <liquid:helium> * 50, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:cooler:1>, <nuclearcraft:cooler>, <liquid:water> * 1000, 800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:cooler:8>, <nuclearcraft:cooler>, <liquid:helium> * 1000, 800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:cooler:10>, <nuclearcraft:cooler>, <liquid:cryotheum> * 2000, 1600);
mods.thermalexpansion.Transposer.addFillRecipe(<thermalfoundation:material:165>, <nuclearcraft:alloy:7>, <liquid:redstone> * 250, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<thermalfoundation:material:166>, <nuclearcraft:alloy:8>, <liquid:glowstone> * 250, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<thermalfoundation:material:167>, <nuclearcraft:alloy:9>, <liquid:ender> * 250, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:thorium:1>, <nuclearcraft:thorium>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:thorium:3>, <nuclearcraft:thorium:2>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:thorium:5>, <nuclearcraft:thorium:4>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:thorium:7>, <nuclearcraft:thorium:6>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:uranium:1>, <nuclearcraft:uranium>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:uranium:3>, <nuclearcraft:uranium:2>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:uranium:5>, <nuclearcraft:uranium:4>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:uranium:7>, <nuclearcraft:uranium:6>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:uranium:9>, <nuclearcraft:uranium:8>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:uranium:11>, <nuclearcraft:uranium:10>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:neptunium:1>, <nuclearcraft:neptunium>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:neptunium:3>, <nuclearcraft:neptunium:2>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:neptunium:5>, <nuclearcraft:neptunium:4>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:neptunium:7>, <nuclearcraft:neptunium:6>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:plutonium:1>, <nuclearcraft:plutonium>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:plutonium:3>, <nuclearcraft:plutonium:2>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:plutonium:5>, <nuclearcraft:plutonium:4>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:plutonium:7>, <nuclearcraft:plutonium:6>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:plutonium:9>, <nuclearcraft:plutonium:8>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:plutonium:11>, <nuclearcraft:plutonium:10>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:plutonium:13>, <nuclearcraft:plutonium:12>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:plutonium:15>, <nuclearcraft:plutonium:14>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:americium:1>, <nuclearcraft:americium>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:americium:3>, <nuclearcraft:americium:2>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:americium:5>, <nuclearcraft:americium:4>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:americium:7>, <nuclearcraft:americium:6>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:americium:9>, <nuclearcraft:americium:8>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:americium:11>, <nuclearcraft:americium:10>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:curium:1>, <nuclearcraft:curium>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:curium:3>, <nuclearcraft:curium:2>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:curium:5>, <nuclearcraft:curium:4>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:curium:7>, <nuclearcraft:curium:6>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:curium:9>, <nuclearcraft:curium:8>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:curium:11>, <nuclearcraft:curium:10>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:curium:13>, <nuclearcraft:curium:12>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:curium:15>, <nuclearcraft:curium:14>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:berkelium:1>, <nuclearcraft:berkelium>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:berkelium:3>, <nuclearcraft:berkelium:2>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:berkelium:5>, <nuclearcraft:berkelium:4>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:berkelium:7>, <nuclearcraft:berkelium:6>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:californium:1>, <nuclearcraft:californium>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:californium:3>, <nuclearcraft:californium:2>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:californium:5>, <nuclearcraft:californium:4>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:californium:7>, <nuclearcraft:californium:6>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:californium:9>, <nuclearcraft:californium:8>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:californium:11>, <nuclearcraft:californium:10>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:californium:13>, <nuclearcraft:californium:12>, <liquid:oxygen> * 400, 4000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:californium:15>, <nuclearcraft:californium:14>, <liquid:oxygen> * 50, 500);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_thorium:1>, <nuclearcraft:fuel_thorium>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_thorium:1>, <nuclearcraft:depleted_fuel_thorium>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_uranium:1>, <nuclearcraft:fuel_uranium>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_uranium:1>, <nuclearcraft:depleted_fuel_uranium>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_uranium:3>, <nuclearcraft:fuel_uranium:2>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_uranium:3>, <nuclearcraft:depleted_fuel_uranium:2>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_uranium:5>, <nuclearcraft:fuel_uranium:4>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_uranium:5>, <nuclearcraft:depleted_fuel_uranium:4>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_uranium:7>, <nuclearcraft:fuel_uranium:6>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_uranium:7>, <nuclearcraft:depleted_fuel_uranium:6>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_neptunium:1>, <nuclearcraft:fuel_neptunium>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_neptunium:1>, <nuclearcraft:depleted_fuel_neptunium>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_neptunium:3>, <nuclearcraft:fuel_neptunium:2>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_neptunium:3>, <nuclearcraft:depleted_fuel_neptunium:2>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_plutonium:1>, <nuclearcraft:fuel_plutonium>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_plutonium:1>, <nuclearcraft:depleted_fuel_plutonium>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_plutonium:3>, <nuclearcraft:fuel_plutonium:2>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_plutonium:3>, <nuclearcraft:depleted_fuel_plutonium:2>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_plutonium:5>, <nuclearcraft:fuel_plutonium:4>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_plutonium:5>, <nuclearcraft:depleted_fuel_plutonium:4>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_plutonium:7>, <nuclearcraft:fuel_plutonium:6>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_plutonium:7>, <nuclearcraft:depleted_fuel_plutonium:6>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_americium:1>, <nuclearcraft:fuel_americium>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_americium:1>, <nuclearcraft:depleted_fuel_americium>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_americium:3>, <nuclearcraft:fuel_americium:2>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_americium:3>, <nuclearcraft:depleted_fuel_americium:2>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_curium:1>, <nuclearcraft:fuel_curium>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_curium:1>, <nuclearcraft:depleted_fuel_curium>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_curium:3>, <nuclearcraft:fuel_curium:2>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_curium:3>, <nuclearcraft:depleted_fuel_curium:2>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_curium:5>, <nuclearcraft:fuel_curium:4>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_curium:5>, <nuclearcraft:depleted_fuel_curium:4>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_curium:7>, <nuclearcraft:fuel_curium:6>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_curium:7>, <nuclearcraft:depleted_fuel_curium:6>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_curium:9>, <nuclearcraft:fuel_curium:8>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_curium:9>, <nuclearcraft:depleted_fuel_curium:8>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_curium:11>, <nuclearcraft:fuel_curium:10>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_curium:11>, <nuclearcraft:depleted_fuel_curium:10>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_berkelium:1>, <nuclearcraft:fuel_berkelium>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_berkelium:1>, <nuclearcraft:depleted_fuel_berkelium>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_berkelium:3>, <nuclearcraft:fuel_berkelium:2>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_berkelium:3>, <nuclearcraft:depleted_fuel_berkelium:2>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_californium:1>, <nuclearcraft:fuel_californium>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_californium:1>, <nuclearcraft:depleted_fuel_californium>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_californium:3>, <nuclearcraft:fuel_californium:2>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_californium:3>, <nuclearcraft:depleted_fuel_californium:2>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_californium:5>, <nuclearcraft:fuel_californium:4>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_californium:5>, <nuclearcraft:depleted_fuel_californium:4>, <liquid:oxygen> * 3200, 12800);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:fuel_californium:7>, <nuclearcraft:fuel_californium:6>, <liquid:oxygen> * 4000, 16000);
mods.thermalexpansion.Transposer.addFillRecipe(<nuclearcraft:depleted_fuel_californium:7>, <nuclearcraft:depleted_fuel_californium:6>, <liquid:oxygen> * 3200, 12800);