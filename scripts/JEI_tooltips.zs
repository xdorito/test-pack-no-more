#name JEI_tooltips.zs
#Author: xDorito

#tooltip ores to show how they are mined
<minecraft:iron_ore>.addTooltip(format.green("Found in Hematite and Limonite veins"));
<minecraft:gold_ore>.addTooltip(format.green("Found in Gold veins"));
<minecraft:coal_ore>.addTooltip(format.green("Found in Coal veins"));
<minecraft:lapis_ore>.addTooltip(format.green("Found in Lapis veins"));
<minecraft:diamond_ore>.addTooltip(format.green("Found in Coal and Beryl veins"));
<minecraft:redstone_ore>.addTooltip(format.green("Found in Cinnabar veins"));
<minecraft:emerald_ore>.addTooltip(format.green("Found in Beryl veins"));
<thermalfoundation:ore:0>.addTooltip(format.green("Found in Malachite and Azurite veins"));
<thermalfoundation:ore:1>.addTooltip(format.green("Found in Cassiterite and Tealite veins"));
<thermalfoundation:ore:2>.addTooltip(format.green("Found in Galena veins"));
<thermalfoundation:ore:3>.addTooltip(format.green("Found in Galena veins"));
<thermalfoundation:ore:4>.addTooltip(format.green("Found in Bauxite veins"));
<thermalfoundation:ore:5>.addTooltip(format.green("Found in Limonite veins"));
<techreborn:ore:2>.addTooltip(format.green("Found in Beryl veins"));
<techreborn:ore:3>.addTooltip(format.green("Found in Beryl veins"));
<libvulpes:ore0:8>.addTooltip(format.green("Found in Osmium veins"));
<thaumcraft:ore_amber>.addTooltip(format.green("Found in Azurite veins"));
<thaumcraft:amber>.addTooltip(format.green("Found in Azurite veins"));
<thaumcraft:ore_cinnabar>.addTooltip(format.green("Found in Cinnabar veins"));
<thaumcraft:ore_quartz>.addTooltip(format.green("Found in Quartz veins"));
<appliedenergistics2:quartz_ore>.addTooltip(format.green("Found in Quartz veins"));
<appliedenergistics2:charged_quartz_ore>.addTooltip(format.green("Found rarely in Quartz veins"));
<appliedenergistics2:material:0>.addTooltip(format.green("Found in Quartz veins"));
<appliedenergistics2:material:1>.addTooltip(format.green("Found rarely in Quartz veins"));
<ebwizardry:crystal_ore>.addTooltip(format.green("Found in Aetherum Ore"));
<immersiveengineering:ore:5>.addTooltip(format.green("Found on other planets"));
<mekanism:oreblock:0>.addTooltip(format.green("Found in Osmium veins"));
<actuallyadditions:block_misc:3>.addTooltip(format.green("Found in Coal veins"));
<thermalfoundation:ore:6>.addTooltip(format.green("Found on other planets"));
<thermalfoundation:ore:7>.addTooltip(format.green("Found on other planets and the moon"));
<nuclearcraft:ore:3>.addTooltip(format.green("Found on other planets"));
<nuclearcraft:ore:5>.addTooltip(format.green("Found on other planets"));
<nuclearcraft:ore:6>.addTooltip(format.green("Found on other planets"));
<nuclearcraft:ore:7>.addTooltip(format.green("Found on other planets"));
<libvulpes:ore0:0>.addTooltip(format.green("Found on other planets and the moon"));
