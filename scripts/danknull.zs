print("loading danknull.zs");

# Emerald /dank/null Panel
recipes.removeShaped(<danknull:dank_null_panel:5>);
recipes.addShaped(<danknull:dank_null_panel:5>,
	[[<botania:storage:1>,                   <nuclearcraft:block_depleted_uranium>, <botania:storage:1>],
	 [<nuclearcraft:block_depleted_uranium>, <minecraft:stained_glass_pane:5>,      <nuclearcraft:block_depleted_uranium>],
	 [<botania:storage:1>,                   <nuclearcraft:block_depleted_uranium>, <botania:storage:1>]]);