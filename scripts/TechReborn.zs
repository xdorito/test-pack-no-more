# Author: Moji

# Alternate recipes for implosion compressor

recipes.remove(<techreborn:implosion_compressor>);
recipes.addShaped(<techreborn:implosion_compressor>, [[<ore:ingotIridium>, 		 <ore:machineBlockAdvanced>, <ore:ingotIridium>],
													  [<ore:circuitAdvanced>, 	 <techreborn:compressor>, 	 <ore:circuitAdvanced>],
													  [<ore:ingotAdvancedAlloy>, <ore:machineBlockAdvanced>, <ore:ingotAdvancedAlloy>]]);
													  
recipes.addShaped(<techreborn:implosion_compressor>, [[<ore:ingotEndSteel>, 				<ore:ingotIridium>, 		<ore:ingotEndSteel>],
													  [<enderio:block_reinforced_obsidian>, <enderio:item_material:54>, <enderio:block_reinforced_obsidian>],
													  [<enderio:item_material:13>, 			<ore:ingotIridium>, 		<enderio:item_material:13>]]);		
													  
recipes.addShaped(<techreborn:implosion_compressor>, [[null, 			   <ore:dustPyrotheum>],
													  [<ore:ingotIridium>, <thermalexpansion:frame>, 		 <ore:ingotIridium>],
													  [<ore:gearEnderium>, <thermalfoundation:material:513>, <ore:gearEnderium>]]);	
													  
recipes.addShaped(<techreborn:implosion_compressor>, [[<ore:ingotIridium>, 		   <ore:circuitUltimate>, 	<ore:ingotIridium>],
													  [<ore:ingotRefinedObsidian>, <mekanism:basicblock:8>, <ore:ingotRefinedObsidian>],
													  [<ore:alloyUltimate>, 	   <ore:circuitUltimate>, 	<ore:alloyUltimate>]]);