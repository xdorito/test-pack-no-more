# Authors: JahYi, Moji

# Quarry card takes iridium alloy plates

recipes.removeShaped(<rftools:shape_card:2>);

recipes.addShaped(<rftools:shape_card:2>, 
	[[<techreborn:plates:37>, <minecraft:diamond_pickaxe>, <techreborn:plates:37>],
	 [<ore:ingotSteel>, 	  <rftools:shape_card>,   	   <ore:ingotSteel>],
	 [<techreborn:plates:37>, <minecraft:diamond_shovel>,  <techreborn:plates:37>]]);
	 
recipes.addShaped(<rftools:shape_card:2>, 
	[[<minecraft:dirt>, <minecraft:dirt>, 		<minecraft:dirt>],
	 [<minecraft:dirt>, <rftools:shape_card:5>, <minecraft:dirt>],
	 [<minecraft:dirt>, <minecraft:dirt>,  		<minecraft:dirt>]]);